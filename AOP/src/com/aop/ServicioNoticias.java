package com.aop;

public interface ServicioNoticias {
	
	public void insertarNoticia(Noticia noticia);
	public void borrarNoticia(Integer id);
	public void updateNoticia(Noticia noticia);
	public Noticia getNoticia(Integer id);

}
