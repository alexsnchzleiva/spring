package com.aop;

import java.lang.reflect.Method;
import java.util.Calendar;

import org.springframework.aop.AfterReturningAdvice;

public class SetFechaAltaAfterReturning implements AfterReturningAdvice {

	@Override
	public void afterReturning(Object returnValue, Method method, Object[] parametros,
			Object target) throws Throwable {

		Noticia noticia = (Noticia) parametros[0];
		noticia.setFechaAlta(Calendar.getInstance().getTime());
	}

}
