package com.aop;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

public class ServicioNoticiasEstaticoImpl implements ServicioNoticias,
		InitializingBean {
	
	private Map<Integer, Noticia> noticias;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.noticias = new HashMap<Integer,Noticia>();
	}

	@Override
	public void insertarNoticia(Noticia noticia) {
		this.noticias.put(noticia.getId(), noticia);
	}

	@Override
	public void borrarNoticia(Integer id) {
		this.noticias.remove(id);
	}

	@Override
	public void updateNoticia(Noticia noticia) {
		this.noticias.remove(noticia.getId());
		this.noticias.put(noticia.getId(), noticia);
	}

	@Override
	public Noticia getNoticia(Integer id) {
		return this.noticias.get(id);
	}

}
