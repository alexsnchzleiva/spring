package com.aop;

import java.lang.reflect.Method;

import org.springframework.aop.ThrowsAdvice;

public class SetFechaAltaAfterThrow implements ThrowsAdvice {

	public void afterThrowing(Exception ex){
		System.out.println(ex.getMessage());
	}
	
	public void afterThrowing(Method method, Object [] args, Object target, Exception ex){
		
	}
	
}
