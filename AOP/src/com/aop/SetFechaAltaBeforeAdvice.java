package com.aop;

import java.lang.reflect.Method;
import java.util.Calendar;

import org.springframework.aop.MethodBeforeAdvice;

public class SetFechaAltaBeforeAdvice implements MethodBeforeAdvice {

	@Override
	public void before(Method method, Object[] parametros, Object target)
			throws Throwable {
		
		Noticia noticia = (Noticia) parametros[0];
		noticia.setFechaAlta(Calendar.getInstance().getTime());
	}
	
}
