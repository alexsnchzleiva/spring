package com.aop;

import java.util.Calendar;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class SetFechaAltaAround implements MethodInterceptor {

	@Override
	public Object invoke(MethodInvocation metodo) throws Throwable {
		
		Noticia noticia = (Noticia) metodo.getArguments()[0];
		noticia.setFechaAlta(Calendar.getInstance().getTime());
		
		return metodo.proceed();
	}

}
