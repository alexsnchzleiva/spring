package com.aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EjemploNoticiasSpring {
	
	public static void main(String [] args){
		
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		ServicioNoticias servicio = (ServicioNoticias) context.getBean("aop.servicio.noticias");
		
		Noticia noticia = new Noticia();
		noticia.setId(1);
		noticia.setTitulo("Titulo 1");
		noticia.setDescripcion("Descripcion 1");
		servicio.insertarNoticia(noticia);
		
		Noticia noticiaObtenida = servicio.getNoticia(1);
		System.out.println("Fecha almacenada: " + noticiaObtenida.getFechaAlta());
	}

}
