package com.ciclo;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		BeanFactory factoria = context;
		BeanPropiedad ejemplo = (BeanPropiedad) factoria.getBean("beanPropiedad");
		ejemplo.verIp();
	}

}
