package com.ciclo;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

public class BeanPropiedad implements InitializingBean , DisposableBean {
	
	private String ipServidor;
	
	public void setIpServidor(String ipServidor){
		this.ipServidor = ipServidor;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(this.ipServidor, "No existe la ip");
	}

	@Override
	public void destroy() throws Exception {		
		System.out.println("Destruimos el Bean");
	}
	
	public void verIp(){
		System.out.println(this.ipServidor);
	}
	
	
	

}
