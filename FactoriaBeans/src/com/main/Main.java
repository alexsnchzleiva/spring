package com.main;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Main {

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		BeanFactory factoria = context;
		IoC ioc = (IoC) factoria.getBean("iocBean");
		ioc.insertarOrden("234", "Alex", "inlandempire");
		
	}

}
