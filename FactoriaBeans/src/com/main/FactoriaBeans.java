package com.main;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;


public class FactoriaBeans implements BeanFactoryAware {
	
	private BeanFactory factoria;

	@Override
	public void setBeanFactory(BeanFactory factoria) throws BeansException {
		this.factoria = factoria;	
	}

}
