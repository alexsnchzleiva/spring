package com.main;


public class IoC {
	
	private ServicioSeguridad seguridad;
	
	
	public void setSeguridad(ServicioSeguridad seguridad) {
		this.seguridad = seguridad;
	}
	
	
	public void insertarOrden(String orden, String user, String pwd){
		this.seguridad.login(user, pwd);
	}
	
}
