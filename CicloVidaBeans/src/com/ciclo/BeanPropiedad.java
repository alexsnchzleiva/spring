package com.ciclo;

import org.springframework.util.Assert;

public class BeanPropiedad {
	
	private String ipServidor;
	
	public void setIpServidor(String ipServidor){
		this.ipServidor = ipServidor;
	}

	public void init() throws Exception {
		Assert.notNull(this.ipServidor, "No existe la ip");
	}

	public void clean() throws Exception {		
		System.out.println("Destruimos el Bean");
	}
	
	public void verIp(){
		System.out.println(this.ipServidor);
	}
	
	
	

}
